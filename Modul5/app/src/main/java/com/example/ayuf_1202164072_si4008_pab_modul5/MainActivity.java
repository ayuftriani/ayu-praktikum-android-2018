package com.example.ayuf_1202164072_si4008_pab_modul5;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class MainActivity extends AppCompatActivity {

    Button btnListArticle, btnCreateArticle, btnSetting;
    TextView tvGood;
    final String judul = "Good Night";

    String PREF_NIGHT_MODE = "NightMode";
    sharedpref mysharedpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mysharedpref = new sharedpref(this);
        if (mysharedpref.loadNightModeState() == true) {
            setTheme(R.style.DarkTheme);
        } else
        {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCreateArticle = findViewById(R.id.btn_create_article);
        btnListArticle = findViewById(R.id.btn_list_article);
        btnSetting = findViewById(R.id.btn_setting);
        tvGood = findViewById(R.id.tv_good_mood);

        btnCreateArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, BuatArtikel.class));
            }
        });

        btnListArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ArtikelList.class));
            }
        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
            }
        });

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) tvGood.setText(judul);
    }
}
