package com.example.ayuf_1202164072_si4008_pab_modul5;

import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ArtikelList extends AppCompatActivity {

    RecyclerView recyclerView;
    ArtikelAdapter adapterArtikel;
    List<ArtikelActivity> artikelActivityList;
    ArtikelDbHelper dbHelper;
    sharedpref mysharedpref;


    final String PREF_NIGHT_MODE = "NightMode";
    SharedPreferences spNight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        mysharedpref = new sharedpref(this);
        if (mysharedpref.loadNightModeState() == true) {
            setTheme(R.style.DarkTheme);
        } else
        {
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artikel_list);

        dbHelper = new ArtikelDbHelper(this);
        recyclerView = findViewById(R.id.rv_article);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        artikelActivityList = new ArrayList<>();

        dbHelper.readData(artikelActivityList);

        adapterArtikel = new ArtikelAdapter(this, artikelActivityList);
        recyclerView.setAdapter(adapterArtikel);

        adapterArtikel.notifyDataSetChanged();
    }
}
