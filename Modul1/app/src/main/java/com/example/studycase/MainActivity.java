package com.example.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText alas;
    private EditText tinggi;
    private TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void cek(View view) {
        alas = findViewById(R.id.alas);
        tinggi = findViewById(R.id.tinggi);
        hasil = findViewById(R.id.hasil);

        Integer data1 = Integer.parseInt(alas.getText().toString());
        Integer data2 = Integer.parseInt(tinggi.getText().toString());
        Integer hasilnya = data1*data2;
        hasil.setText(String.valueOf(hasilnya));


    }

}
